///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.werapan.databaseproject.model;
//
//import java.sql.Date;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// *
// * @author ทักช์ติโชค
// */
//public class OrdersEdit {
//
//    private int id;
//    private Date date;
//    private double total;
//    private int qty;
//    private ArrayList<OrderDetail> orderDatails;
//
//    public OrdersEdit(int id, Date date, double total, int qty, ArrayList<OrderDetail> orderDatails) {
//        this.id = id;
//        this.date = date;
//        this.total = total;
//        this.qty = qty;
//        this.orderDatails = orderDatails;
//    }
//
//    public OrdersEdit() {
//        this.id = -1;
//        orderDatails = new ArrayList<>();
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public Date getDate() {
//        return date;
//    }
//
//    public void setDate(Date date) {
//        this.date = date;
//    }
//
//    public double getTotal() {
//        return total;
//    }
//
//    public void setTotal(double total) {
//        this.total = total;
//    }
//
//    public int getQty() {
//        return qty;
//    }
//
//    public void setQty(int qty) {
//        this.qty = qty;
//    }
//
//    public ArrayList<OrderDetail> getOrderDatails() {
//        return orderDatails;
//    }
//
//    public void setOrderDatails(ArrayList<OrderDetail> orderDatails) {
//        this.orderDatails = orderDatails;
//    }
//
//    @Override
//    public String toString() {
//        return "Orders{" + "id=" + id + ", date=" + date + ", total=" + total + ", qty=" + qty + '}';
//    }
//
//    public void addOrderDetail(OrderDetail orderDetail) {
//        orderDatails.add(orderDetail);
//        total = total + orderDetail.getTotal();
//        qty = qty + orderDetail.getQty();
//    }
//
//    public void addOrderDetail(Product product, String productName, double productPrice, int qty) {
//        OrderDetail orderDetail = new OrderDetail(product, productName, productPrice, qty, this);
//        this.addOrderDetail(orderDetail);
//    }
//
//    public void addOrderDetail(Product product, int qty) {
//        OrderDetail orderDetail = new OrderDetail(product, product.getName(), product.getPrice(), qty, this);
//        this.addOrderDetail(orderDetail);
//    }
//
//    public static OrdersEdit fromRS(ResultSet rs) {
//        OrdersEdit order = new OrdersEdit();
//        try {
//            order.setId(rs.getInt("order_id"));
//            order.setQty(rs.getInt("order_qty"));
//            order.setTotal(rs.getDouble("order_total"));
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            order.setDate((Date) sdf.parse(rs.getString("order_date")));
//        } catch (SQLException ex) {
//            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
//            return null;
//        } catch (ParseException ex) {
//            Logger.getLogger(OrdersEdit.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return order;
//    }
//
//}
